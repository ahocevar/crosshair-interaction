import Modify from "ol/interaction/Modify.js";
import VectorSource from "ol/source/Vector.js";
import Feature from "ol/Feature.js";
import LineString from "ol/geom/LineString.js";
import VectorLayer from "ol/layer/Vector.js";
import { style } from "./style.js";
import { FALSE } from "ol/functions";
import { equals } from "ol/coordinate";


/**
 * 
 * @param {import("ol/Map.js").default} map Map instance the interaction will be added to
 * @param {import("ol/coordinate").Coordinate} coord1 First coordinate in map projection
 * @param {import("ol/coordinate").Coordinate} coord2 Second coordinate in map projection
 * @param {import("ol/coordinate").Coordinate} coord3 Third coordinate in map projection
 * @param {function(import("ol/coordinate").Coordinate, import("ol/coordinate").Coordinate, import("ol/coordinate").Coordinate):void} onChange Callback function when coordinates have changed
 * @returns {import("ol/interaction/Modify.js").default} The interaction
 */
export function addInteraction(map, coord1, coord2, coord3, onChange = () => {}) {
  const feature = new Feature(new LineString([coord1, coord2, coord3]));
  const source = new VectorSource({
    features: [feature]
  });
  const layer = new VectorLayer({
    source,
    style: style()
  });
  const modify = new Modify({
    source: source,
    style: FALSE,
    insertVertexCondition: FALSE,
    deleteCondition: FALSE
  });
  map.addInteraction(modify);

  const overlaySource = modify.getOverlay().getSource();
  let modifying = false;
  modify.on(['modifystart', 'modifyend'], (evt) => {
    modifying = evt.type === 'modifystart';
    if (evt.type === 'modifyend') {
      map.getTargetElement().style.cursor = 'pointer';
    }
  });
  overlaySource.on(['addfeature', 'changefeature', 'removefeature'], (evt) => {
    const vertex = overlaySource.getFeatures()[0];
    let vertexCoord = vertex && vertex.getGeometry().getCoordinates()
    map.getTargetElement().style.cursor =
      vertexCoord && feature.getGeometry().getCoordinates().find(coord => equals(coord, vertexCoord))
        ? modifying ? 'grabbing' : 'pointer'
        : '';
  });
  modify.on('modifyend', (e) => {
    if (e.features.getLength() > 0) {
      const coordinates = e.features.item(0).getGeometry().getCoordinates();
      onChange(...coordinates);  
    }
  });
  modify.on('change:active', () => {
    if (modify.getActive()) {
      layer.setMap(map);
    } else {
      layer.setMap(undefined);
    }
  });
  layer.setMap(map);

  return modify;
}
