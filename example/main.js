import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { addInteraction } from '../index';
import { fromLonLat } from 'ol/proj';
const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  view: new View({
    center: fromLonLat([16.4586833359451, 48.19054474132315]),
    zoom: 15
  })
});

const interaction = addInteraction(map,
  fromLonLat([16.471570602480117, 48.18201275410553]),
  fromLonLat([16.4586833359451, 48.19054474132315]),
  fromLonLat([16.44887190065783, 48.197732602752865]),
  (coord1, coord2, coord3) => console.log(coord1, coord2, coord3)
);  

const crosshair = document.getElementById('crosshair');
crosshair.addEventListener('click', function() {
  const action = crosshair.innerText;
  if (action === 'Turn editing off') {
    crosshair.innerText = 'Turn editing on';
    interaction.setActive(false);
  } else {
    crosshair.innerText = 'Turn editing off';
    interaction.setActive(true);
  }
});
