# 3-crosshair interaction

## Usage

The component resides in `index.js`. See the JSDoc of the exported `addInteraction()` function for details, and the example (`example/main.js`).

## Development and example

    npm install
    npm start

The example will be available at http://localhost:8080/. The code is built with a source map for easy debugging.