import Point from "ol/geom/Point.js";
import {Style, Stroke, Icon} from "ol/style.js";
import theme from "./theme.js";

export function style() {
  return function (feature) {
    let geometry = feature.getGeometry();

    //line stroke style
    let styles = [
      new Style({
        stroke: new Stroke({
          color: theme.mrtisOrange,
          lineDash: [5, 5],
          width: 2
        })
      })
    ];

    //crosshairs
    const segmentCount = geometry.getCoordinates().length;
    let currentSegment = 0;
    geometry.forEachSegment(function (start, end) {
      currentSegment++;
      let dx = end[0] - start[0];
      let dy = end[1] - start[1];
      let rotation = Math.atan2(dy, dx);

      styles.push(new Style({
        geometry: new Point(start),
        image: new Icon({
          //the measure arrow tip must be centered within the svg file
          //this ensures that rotation around center anchor remains at measure click location
          src: './public/svg/crosshair.svg',
          anchor: [0.5, 0.5],
          imgSize: [100, 100],      //imgSize required IE11
          scale: 0.25,
          rotateWithView: true,
          rotation: -rotation + Math.PI / 2
        })
      }));
      if (currentSegment === segmentCount - 1) {
        styles.push(new Style({
          geometry: new Point(end),
          image: new Icon({
            //the measure arrow tip must be centered within the svg file
            //this ensures that rotation around center anchor remains at measure click location
            src: './public/svg/crosshair.svg',
            anchor: [0.5, 0.5],
            imgSize: [100, 100],      //imgSize required IE11
            scale: 0.25,
            rotateWithView: true,
            rotation: -rotation
          })
        }));
      }
    });

    return styles;
  };
}