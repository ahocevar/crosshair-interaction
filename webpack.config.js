const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
  entry: path.join(__dirname, 'example', 'main.js'),
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'index_bundle.js',
  },
  devtool: 'source-map',
  mode: process.env.NODE_ENV || 'none',
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [new CopyPlugin({
    patterns: [
      { from: "public", to: "public" },
    ],
  }), new HtmlWebpackPlugin({
    template: './example/index.html'
  })],
};